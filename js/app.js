"use strict";

const doRangeSlider = function (selector) {
    const rangeSliderCollections = $(selector);
    if (rangeSliderCollections.length) {
        rangeSliderCollections.each(function (idx, item) {
            const rangeSlider = $(item);
            const minValue = + rangeSlider.attr("data-min-value");
            const maxValue = + rangeSlider.attr("data-max-value");
            const step = + rangeSlider.attr("data-step");
            const $rangeSliderWrap = rangeSlider.closest('.range-slider-wrap');
            const $fromInput = $rangeSliderWrap.find('.from-input');
            const $toInput = $rangeSliderWrap.find('.to-input');

            rangeSlider.slider({
                range: true,
                min: minValue,
                max: maxValue,
                step: step ? step : 1,
                values: [ minValue, maxValue ],
                slide: function( event, ui ) {
                    $fromInput.val(ui.values[0]);
                    $toInput.val(ui.values[1]);
                }
            });

            $fromInput.on('change', function () {
                const val = + $(this).val();
                let res =  !isNaN(val) &&  val >= rangeSlider.slider( "option", "min" )  ? val : rangeSlider.slider( "option", "min" );
                $(this).val(res);
                rangeSlider.slider( "values", [ res, rangeSlider.slider( "values", 1 )] );
            });
            $toInput.on('change', function () {
                const val = + $(this).val();
                let res =  !isNaN(val) &&  val <= rangeSlider.slider( "option", "max" )  ? val : rangeSlider.slider( "option", "max" );
                $(this).val(res);
                rangeSlider.slider( "values", [ rangeSlider.slider( "values", 0 ), res ] );
            });




            $( ".selector" ).slider( "values", [ 55, 105 ] );
        })
    }
};

const doUserSelect = function (selector) {
    const userSelect = $(selector);
    if (userSelect.length) {
        userSelect.select2({
            minimumResultsForSearch: Infinity,
        });
    }
}

const doMainPresentationSlider = function(selector) {
    const $mainPresentationSlider = $(selector);
    if (!$mainPresentationSlider.length > 0) return false;
    const sliderOptions = {
        dots: false,
        arrows: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: true,
        autoplay: true,
        autoplaySpeed:3000,
        pauseOnHover:false
    };
    $mainPresentationSlider.each(function (idx, item) {
        const slider = $(item);
        const sliderWrap = slider.closest('.slider-wrap');
        const prevArrow = sliderWrap.find('.btn-slider.prev');
        const nextArrow = sliderWrap.find('.btn-slider.next');
        const circle =  sliderWrap.find('.btn-slider-round-icon circle.path-round-line');

        const newOptions = Object.assign(sliderOptions, {
            prevArrow: prevArrow,
            nextArrow: nextArrow,
        })

        slider.slick(newOptions);

        const animationDelay = newOptions.autoplaySpeed + 'ms';
        const animationCss = {
            'animation-duration': animationDelay,
            '-webkit-animation-duration': animationDelay,
            'animation-name': 'round-loading',
            '-webkit-animation-name': 'round-loading',
            'animation-timing-function': 'linear',
            '-webkit-animation-timing-function': 'linear',
            'animation-delay': `${animationDelay}ms`,
            '-webkit-animation-delay': `${animationDelay}ms`
        }

        const mainOverlaySlides =  $('.main-overlay-slider .main-overlay-slide');

        slider.on('beforeChange', function(event, slick, currentSlide, nextSlide){
            circle.attr('style', ' ');
            let setTime = setTimeout(function () {
                circle.css(animationCss);
                if (mainOverlaySlides && mainOverlaySlides.length) {
                    mainOverlaySlides.eq(currentSlide).removeClass('active');
                    mainOverlaySlides.eq(nextSlide).addClass('active');
                }
                clearTimeout(setTime);
            }, 10);
        });
    });
};

const doProjectLinkLogic = function () {
    const $mainProjectLink = $('.main-project-link');
    const mainProjectsWrap = $('.main-block-projects-wrap');

    if ($mainProjectLink.length) {
        const openProjectLayerHandler = function (evt) {
            const projectLayerId =  $(evt.target).attr('data-project-id');
            const projectLayer = $(`#${projectLayerId}`);
            if (projectLayer.length) {
                mainProjectsWrap.addClass('projects-layer-opened');
                projectLayer.addClass('active');
            }
        }
        const hideProjectLayerHandler = function (evt) {
            const projectLayerId =  $(evt.target).attr('data-project-id');
            const projectLayer =  $(`#${projectLayerId}`);
            if (projectLayer.length) {
                mainProjectsWrap.removeClass('projects-layer-opened');
                projectLayer.removeClass('active');
            }
        }
        $(document).on('mouseover', '.main-project-link', openProjectLayerHandler);
        $(document).on('mouseout', '.main-project-link', hideProjectLayerHandler);
    }
}

$(document).ready(function () {
    doRangeSlider(".range-slider");
    doMainPresentationSlider('.main-presentation-slider');
    doUserSelect('.user-select');
    doProjectLinkLogic();
});
