<?php header("Content-Type: text/html; charset=utf-8");?>
<!DOCTYPE html>
<html lang="ru-RU">
<head>
    <?php include "parts/header.php"; ?>
</head>
<body>
<div class="main-wrap">
    <div class="main-block main-block-projects-wrap">

        <div class="main-overlay-slider">
            <div class="main-overlay-slide active"><img src="img/project-layer-1.jpg" alt="" class="main-overlay-slide-content">
                <video muted="" autoplay="" loop="" preload="none" class="main-overlay-slide-content">
                    <source src="videos/video-1.mp4" type="video/mp4">
                </video>
            </div>
            <div class="main-overlay-slide">
                <video  muted="" autoplay="" loop="" preload="none" class="main-overlay-slide-content">
                    <source src="videos/video-2.mp4" type="video/mp4">
                </video>
            </div>
        </div>

        <ul class="stick-soc-box">
            <li class="stick-soc-item">
                <a href="#" target="_blank" class="stick-soc-unit">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24">
                        <use xlink:href="img/svg-sprites/icons-sprite.svg#facebook-social-icon"></use>
                    </svg>
                </a>
            </li>
            <li class="stick-soc-item">
                <a href="#" target="_blank" class="stick-soc-unit">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24">
                        <use xlink:href="img/svg-sprites/icons-sprite.svg#instagram-social-icon"></use>
                    </svg>
                </a>
            </li>
            <li class="stick-soc-item">
                <a href="#" target="_blank" class="stick-soc-unit">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24">
                        <use xlink:href="img/svg-sprites/icons-sprite.svg#twitter-social-icon"></use>
                    </svg>
                </a>
            </li>
            <li class="stick-soc-item">
                <a href="#" target="_blank" class="stick-soc-unit">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24">
                        <use xlink:href="img/svg-sprites/icons-sprite.svg#vk-social-icon"></use>
                    </svg>
                </a>
            </li>
        </ul>
        <header class="header-block">
            <div class="container">
                <div class="header">
                    <a href="/" class="header-logo"><img src="img/logo.svg" width="198" height="21" alt="СУВАР СТРОИТ"></a>

                    <div class="head-contacts">
                        <p class="head-contacts__phone">
                            <a href="tel:+78002223790" class="head-phone-unit">8 800 222-37-90</a>
                        </p>
                        <p class="head-contacts__desc">Работаем ежедневно с 10:00 до 20:00</p>
                    </div>

                    <div class="head-nav">
                        <ul class="nav-list">
                            <li class="nav-list__item"><a href="#" data-label-hover="Квартиры" class="nav-link"><span>Квартиры</span></a></li>
                            <li class="nav-list__item"><a href="#" data-label-hover="Ипотека" class="nav-link"><span>Ипотека</span></a></li>
                            <li class="nav-list__item"><a href="#" data-label-hover="Коммерция" class="nav-link"><span>Коммерция</span></a></li>
                            <li class="nav-list__item"><a href="#" data-label-hover="Управляющая компания" class="nav-link"><span>Управляющая компания</span></a></li>
                        </ul>
                    </div>

                    <button type="button" class="btn-menu"></button>
                </div>
            </div>
        </header>

        <div class="main-block-body container">
            <a class="sticker-label" href="https://дом.рф" target="_blank">
                <img src="img/dom-rf-label.png" alt="ДОМ РФ" srcset="img/dom-rf-label@2x.png 2x" width="205" height="55" class="sticker-label-img-desktop">
                <img src="img/dom-rf-label-mobile.png" alt="ДОМ РФ" srcset="img/dom-rf-label-mobile@2x.png 2x" width="58" height="55" class="sticker-label-img-mobile">
            </a>
            
            <div class="main-projects-block">

                <ul class="main-projects-list">
                    <li class="main-projects-item"><a href="#" class="main-project-link" data-project-id="project-1">Южный парк</a></li>
                    <li class="main-projects-item"><a href="#" class="main-project-link" data-project-id="project-1">Столичный</a></li>
                    <li class="main-projects-item"><a href="#" class="main-project-link" data-project-id="project-2">Станция Спортивная</a></li>
                    <li class="main-projects-item"><a href="#" class="main-project-link" data-project-id="project-2">Залесный сити</a></li>
                    <li class="main-projects-item"><a href="#" class="main-project-link" data-project-id="project-1">Времена года</a></li>
                    <li class="main-projects-item"><a href="#" class="main-project-link" data-project-id="project-1">Манхэттен</a></li>
                    <li class="main-projects-item"><a href="#" class="main-project-link" data-project-id="project-2">Сказочный лес</a></li>
                    <li class="main-projects-item"><a href="#" class="main-project-link" data-project-id="project-2">Август Астры</a></li>
                    <li class="main-projects-item"><a href="#" class="main-project-link" data-project-id="project-1">Палитра</a></li>
                </ul>

                <div class="project-layer-box">
                    <div class="project-layer" id="project-1">
                        <div class="project-layer-cover">
                            <img src="img/project-layer-1.jpg" alt="" width="765" height="474">
                        </div>
                        <div class="project-layer-text">
                            <p class="project-layer-title">Проект 1</p>
                            <p class="project-layer-desc">Описание проекта</p>
                        </div>
                    </div>
                    <div class="project-layer" id="project-2">
                        <div class="project-layer-cover">
                            <img src="img/project-layer-2.jpg" alt="" width="765" height="474">
                        </div>
                        <div class="project-layer-text">
                            <p class="project-layer-title">Проект 2</p>
                            <p class="project-layer-desc">Какой-нибудь текст</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="main-presentation-slider-wrap slider-wrap">
                <div class="main-presentation-slider">
                    <div class="main-presentation-slide">
                        <div class="main-presentation-slide__text">
                            Стильная архитектура и&nbsp;виды из&nbsp;окон будущей квартиры не&nbsp;оставят равнодушных
                        </div>
                        <div class="main-presentation-slide__btn-wrap btn-group">
                            <div class="btn-group-item">
                                <a href="#" class="btn btn-user btn-outline btn-prev-plus"><span class="btn-inner">Выбрать квартиру</span></a>
                            </div>
                            <div class="btn-group-item">
                                <a href="#" class="btn btn-user btn-arrow"><span class="btn-inner">Купить квартиру онлайн</span></a>
                            </div>
                        </div>
                    </div>
                    <div class="main-presentation-slide">
                        <div class="main-presentation-slide__text">
                            Стильная архитектура и&nbsp;виды из&nbsp;окон будущей квартиры не&nbsp;оставят равнодушных 2
                        </div>
                        <div class="main-presentation-slide__btn-wrap btn-group">
                            <div class="btn-group-item">
                                <a href="#" class="btn btn-user btn-outline btn-prev-plus"><span class="btn-inner">Выбрать квартиру</span></a>
                            </div>
                            <div class="btn-group-item">
                                <a href="#" class="btn btn-user btn-arrow"><span class="btn-inner">Купить квартиру онлайн</span></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="btn-slider-group">
                    <div class="btn-slider  prev">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" class="btn-slider-icon">
                            <use xlink:href="img/svg-sprites/icons-sprite.svg#chevron-icon"></use>
                        </svg>
                    </div>
                    <div class="btn-slider next btn-slider-round">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" class="btn-slider-icon">
                            <use xlink:href="img/svg-sprites/icons-sprite.svg#chevron-icon"></use>
                        </svg>
                        <svg xmlns="http://www.w3.org/2000/svg" width="44" height="44" class="btn-slider-round-icon" viewBox="0 0 44 44">
                            <circle cx="22" cy="22" r="20" stroke="#FFF" stroke-width="2" fill="transparent" class="path-round-line" <!--stroke-dasharray="128" stroke-dashoffset="0"--> />
                        </svg>

                    </div>
                </div>
            </div>


        </div>

        <div class="main-block-footer container">
            <div class="filter-group">
                <div class="filter-unit">
                    <p class="filter-unit-label">Количество комнат</p>
                    <div class="checkbox-group">
                        <label class="checkbox-control">
                            <input type="checkbox" value="cтудия" name="count-rooms">
                            <span class="checkbox-control-view">
                            <span class="checkbox-control-view__inner">cтудия</span>
                        </span>
                        </label>
                        <label class="checkbox-control">
                            <input type="checkbox" value="1">
                            <span class="checkbox-control-view">
                            <span class="checkbox-control-view__inner">1</span>
                        </span>
                        </label>
                        <label class="checkbox-control">
                            <input type="checkbox" value="2">
                            <span class="checkbox-control-view">
                            <span class="checkbox-control-view__inner">2</span>
                        </span>
                        </label>
                        <label class="checkbox-control">
                            <input type="checkbox" value="3">
                            <span class="checkbox-control-view">
                            <span class="checkbox-control-view__inner">3</span>
                        </span>
                        </label>
                        <label class="checkbox-control">
                            <input type="checkbox" value="4">
                            <span class="checkbox-control-view">
                            <span class="checkbox-control-view__inner">4</span>
                        </span>
                        </label>
                    </div>
                </div>
                <div class="filter-unit">
                    <p class="filter-unit-label">Площадь м<sup>2</sup></p>
                    <div class="range-slider-wrap">
                        <div class="range-slider-values-group">
                            <div class="range-slider-value" data-value-prefix="от">
                                <input type="text" name="from-square" class="from-input square-mask">
                            </div>
                            <div class="range-slider-value" data-value-prefix="до">
                                <input type="text" name="to-square" class="to-input square-mask">
                            </div>
                        </div>
                        <div class="range-slider" data-min-value="0" data-max-value="200"></div>
                    </div>
                </div>
                <div class="filter-unit">
                    <p class="filter-unit-label">Стоимость, млн&nbsp;₽</p>
                    <div class="range-slider-wrap">
                        <div class="range-slider-values-group">
                            <div class="range-slider-value " data-value-prefix="от">
                                <input type="text" name="from-cost" class="from-input cost-mask ">
                            </div>
                            <div class="range-slider-value " data-value-prefix="до">
                                <input type="text" name="to-cost" class="to-input cost-mask">
                            </div>
                        </div>
                        <div class="range-slider" data-min-value="2.5" data-max-value="14" data-step="0.1"></div>
                    </div>
                </div>
                <div class="filter-unit">
                    <p class="filter-unit-label">Жилой комплекс</p>

                    <select name="complex" class="user-select">
                        <option value="Все" selected>Все</option>
                        <option value="Южный парк">Южный парк</option>
                        <option value="Столичный">Столичный</option>
                        <option value="Станция Спортивная">Станция Спортивная</option>
                        <option value="Залесный сити">Залесный сити</option>
                        <option value="Времена года">Времена года</option>
                        <option value="Манхэттен">Манхэттен</option>
                        <option value="Сказочный лес">Сказочный лес</option>
                        <option value="Август Астры">Август Астры</option>
                        <option value="Палитра">Палитра</option>
                    </select>
                </div>
                <div class="filter-unit filter-unit-btn-settings">
                    <button type="button" class="btn btn-user btn-outline btn-settings"><span class="btn-inner">Еще <br>фильтры</span></button>
                </div>
                <div class="filter-unit">
                    <button type="button" class="btn btn-user btn-fill"><span class="btn-inner">Показать 120 квартир</span></button>
                </div>
            </div>

            <button type="button" class="btn-search btn-user">
                <span class="btn-inner">Подобрать квартиру</span>
            </button>

        </div>

    </div>
    


    


</div>




<?php include "parts/footer.php"; ?>
</body>
</html>